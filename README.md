# fastify-docker

This is a simple example of how to use Fastify with Docker.

## Local

```bash
pnpm install
```

```bash
pnpm start
```

## Docker

```bash
docker build -t fastify:latest .
docker run -p 3000:3000 fastify:latest
```
