// const fastify = require('fastify')();



// fastify.get('/', async (request, reply) => {
//   return { message: 'Hello world!' }
// })

// fastify.listen({ host: ADDRESS, port: parseInt(PORT, 10) }, (err, address) => {
//   if (err) {
//     console.error(err)
//     process.exit(1)
//   }
//   console.log(`Server listening at ${address}`)
// })

// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })

const { ADDRESS = 'localhost', PORT = '3000' } = process.env;

// Declare a route
fastify.get('/', async (request, reply) => {
  return { hello: 'world' }
})

// Run the server!
const start = async () => {
  try {
    await fastify.listen({
      host: ADDRESS,
      port: parseInt(PORT, 10)
    })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()